import tensorflow as tf
import numpy as np


class Logger(object):
    def __init__(self, sess, logdir):
        self.sess = sess
        self.pg_losses = []
        self.vf_losses = []
        self.ent_losses = []
        self.total_losses = []
        self.rewards = []

        self.logdir = logdir
        self.writer = tf.summary.FileWriter(self.logdir, tf.get_default_graph())
        self._summaries()

    def _summaries(self):
        self.pg_loss = [[],[]]
        self.vf_loss = [[],[]]
        self.entropy_loss = [[],[]]
        self.total_loss = [[],[]]
        self.lr = [[],[]]
        self.clip_param = [[],[]]
        self.merged_loss_sum = [[],[]]
        
        for a in range(2):
            scope = "agent" + str(a) + "/"
            self.pg_loss[a] = tf.placeholder(tf.float32)
            pg_loss_sum = tf.summary.scalar(scope+'Policy Loss', self.pg_loss[a])

            self.vf_loss[a] = tf.placeholder(tf.float32)
            vf_loss_sum = tf.summary.scalar(scope+'Value Function Loss', self.vf_loss[a])

            self.entropy_loss[a] = tf.placeholder(tf.float32)
            entropy_loss_sum = tf.summary.scalar(scope+'Entropy Loss', self.entropy_loss[a])

            self.total_loss[a] = tf.placeholder(tf.float32)
            total_loss_sum = tf.summary.scalar(scope+'Total Loss', self.total_loss[a])

            self.lr[a] = tf.placeholder(tf.float32)
            lr_sum = tf.summary.scalar(scope+'Learning Rate', self.lr[a])

            self.clip_param[a] = tf.placeholder(tf.float32)
            cp_sum = tf.summary.scalar(scope+'Clipping Parameter', self.clip_param[a])

            self.merged_loss_sum[a] = tf.summary.merge([pg_loss_sum, vf_loss_sum, entropy_loss_sum, total_loss_sum, lr_sum, cp_sum])

    def log_losses(self, pg_loss, vf_loss, entropy_loss, total_loss, lr, clip_param, frame):
        self.pg_losses.append(pg_loss)
        self.vf_losses.append(vf_loss)
        self.ent_losses.append(entropy_loss)
        self.total_losses.append(total_loss)

        for a in range(2):
            merged_loss_sum = self.sess.run(self.merged_loss_sum[a],
                                            feed_dict={ self.pg_loss[a]: pg_loss[a],
                                                        self.vf_loss[a]: vf_loss[a],
                                                        self.entropy_loss[a]: entropy_loss[a],
                                                        self.total_loss[a]: total_loss[a],
                                                        self.lr[a]: lr,
                                                        self.clip_param[a]: clip_param})
            self.writer.add_summary(merged_loss_sum, frame)

    def log_console(self, frame):
        print('--------')
        print('Frame:           {}'.format(frame))
        print('Policy Loss:     {:.3f}'.format(self.pg_losses[-1][0]))
        print('Value Loss:      {:.3f}'.format(self.vf_losses[-1][0]))
        print('Entropy Loss:    {:.3f}'.format(self.ent_losses[-1][0]))
        print('Total Loss:      {:.3f}'.format(self.total_losses[-1][0]))
        print('--------')

