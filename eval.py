import sys
import argparse
import math
from operator import itemgetter

import tensorflow as tf
import numpy as np
import gym
import gym_compete

from policy import MlpPolicyValue
from critic import Critic
from actor import Actor
from logger import Logger
import utils


def parse_args():
    parser = argparse.ArgumentParser(description="Environments for Multi-agent competition")

    parser.add_argument("--env", default="run-to-goal-ants", type=str,
                        help="competitive environment: run-to-goal-humans, run-to-goal-ants,"\
                            " you-shall-not-pass, sumo-humans, sumo-ants, kick-and-defend")
    
    parser.add_argument("--param-paths", nargs='+', required=True, type=str)

    parser.add_argument("--max-episodes", default=10, help="max number of matches", type=int)

    args = parser.parse_args()

    return args

def run(config):
    env = utils.get_env(config.env)

    ob_space = env.observation_space.spaces[0].shape
    ac_space = env.action_space.spaces[0].shape
    
    tf_config = tf.ConfigProto(inter_op_parallelism_threads=1, intra_op_parallelism_threads=1)
    sess = tf.Session(config=tf_config)
    sess.__enter__()

    policies = []
    for a in range(2):
        policy = MlpPolicyValue(sess=sess, scope="policy"+str(a),
                                ob_space=ob_space, ac_space=ac_space,
                                logdir="", normalize=True)
        policy.load_model(config.param_paths[a])
        policies.append(policy)

    # initialize uninitialized variables
    sess.run(tf.variables_initializer(tf.global_variables()))


    max_episodes = config.max_episodes
    num_episodes = 0
    nstep = 0
    total_reward = [0.0  for _ in range(len(policies))]
    total_scores = [0 for _ in range(len(policies))]
    # total_scores = np.asarray(total_scores)
    observation = env.reset()
    print("-"*5 + " Episode %d " % (num_episodes+1) + "-"*5)
    while num_episodes < max_episodes:
        env.render()
        action = tuple([policies[i].act(observation=observation[i][None])[0]
                        for i in range(len(policies))])
        observation, reward, done, infos = env.step(action)
        nstep += 1
        for i in range(len(policies)):
            total_reward[i] += reward[i]
        if done[0]:
            num_episodes += 1
            draw = True
            for i in range(len(policies)):
                if 'winner' in infos[i]:
                    draw = False
                    total_scores[i] += 1
                    print("Winner: Agent {}, Scores: {}, Total Episodes: {}".format(i, total_scores, num_episodes))
            if draw:
                print("Game Tied: Agent {}, Scores: {}, Total Episodes: {}".format(i, total_scores, num_episodes))
            observation = env.reset()
            nstep = 0
            total_reward = [0.0  for _ in range(len(policies))]
            for i in range(len(policies)):
                policies[i].reset()
            if num_episodes < max_episodes:
                print("-"*5 + "Episode %d" % (num_episodes+1) + "-"*5)



if __name__ == "__main__":
    config = parse_args()
    run(config)

