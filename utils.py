import gym
import gym_compete

def get_env(env_name):
    if env_name == "kick-and-defend":
        env = gym.make("kick-and-defend-v0")
    elif env_name == "run-to-goal-humans":
        env = gym.make("run-to-goal-humans-v0")
    elif env_name == "run-to-goal-ants":
        env = gym.make("run-to-goal-ants-v0")
    elif env_name == "you-shall-not-pass":
        env = gym.make("you-shall-not-pass-humans-v0")
    elif env_name == "sumo-humans":
        env = gym.make("sumo-humans-v0")
    elif env_name == "sumo-ants":
        env = gym.make("sumo-ants-v0")
    else:
        print("unsupported environment")
        print("choose from: run-to-goal-humans, run-to-goal-ants, you-shall-not-pass," \
            " sumo-humans, sumo-ants, kick-and-defend")
        sys.exit()
    return env
