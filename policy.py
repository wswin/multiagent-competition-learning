import logging
import copy
import asyncio
import os

import tensorflow as tf
import numpy as np
import gym
from tensorflow.contrib import layers


class Policy(object):
    def reset(self, **kwargs):
        pass

    def act(self, observation):
        # should return act, info
        raise NotImplementedError()

class RunningMeanStd(object):
    def __init__(self, scope="running", reuse=False, epsilon=1e-2, shape=()):
        with tf.variable_scope(scope, reuse=reuse):
            self._sum = tf.get_variable(
                dtype=tf.float32,
                shape=shape,
                initializer=tf.constant_initializer(0.0),
                name="sum", trainable=False)
            self._sumsq = tf.get_variable(
                dtype=tf.float32,
                shape=shape,
                initializer=tf.constant_initializer(epsilon),
                name="sumsq", trainable=False)
            self._count = tf.get_variable(
                dtype=tf.float32,
                shape=(),
                initializer=tf.constant_initializer(epsilon),
                name="count", trainable=False)
            self.shape = shape

            self.mean = tf.to_float(self._sum / self._count)
            var_est = tf.to_float(self._sumsq / self._count) - tf.square(self.mean)
            self.std = tf.sqrt(tf.maximum(var_est, 1e-2))


def dense(x, size, name, weight_init=None, bias=True):
    w = tf.get_variable(name + "/w", [x.get_shape()[1], size], initializer=weight_init)
    ret = tf.matmul(x, w)
    if bias:
        b = tf.get_variable(name + "/b", [size], initializer=tf.zeros_initializer())
        return ret + b
    else:
        return ret


class MlpPolicyValue(Policy):
    def __init__(self, sess, scope, ob_space, ac_space, logdir, hiddens=[64, 64], reuse=False,
        normalize=False):

        self.sess = sess
        self.scope = scope
        self.ob_space = ob_space
        self.ac_space = ac_space
        self.hiddens = hiddens
        self.logdir = logdir
        self.reuse = reuse
        self.normalized = normalize
        
        with tf.variable_scope(self.scope, reuse=reuse):
            self._placeholders()
            self._network()
            self.saver = tf.train.Saver(max_to_keep=3)

    def _placeholders(self):
        self.observation_ph = tf.placeholder(tf.float32, (None, *self.ob_space), "observation")

    def _network(self):
        self.zero_state = np.zeros(1)
        if self.normalized:
            if self.normalized != 'ob':
                self.ret_rms = RunningMeanStd(scope="retfilter")
            self.ob_rms = RunningMeanStd(shape=self.ob_space, scope="obsfilter")

        obz = self.observation_ph
        if self.normalized:
            obz = tf.clip_by_value((self.observation_ph - self.ob_rms.mean) / self.ob_rms.std, -5.0, 5.0)

        last_out = obz
        for i, hid_size in enumerate(self.hiddens):
            last_out = tf.nn.tanh(dense(last_out, hid_size, "vffc%i" % (i + 1)))
        self.vpredz = dense(last_out, 1, "vffinal")[:, 0]

        self.vpred = self.vpredz
        if self.normalized and self.normalized != 'ob':
            self.vpred = self.vpredz * self.ret_rms.std + self.ret_rms.mean  # raw = not standardized

        last_out = obz
        for i, hid_size in enumerate(self.hiddens):
            last_out = tf.nn.tanh(dense(last_out, hid_size, "polfc%i" % (i + 1)))
        mean = dense(last_out, self.ac_space[0], "polfinal")
        logstd = tf.get_variable(name="logstd", shape=[1, self.ac_space[0]], initializer=tf.zeros_initializer())

        self.pd = tf.distributions.Normal(mean, tf.exp(logstd))
        self.sampled_action = self.pd.sample()
        self.logprob = self.get_logprob(self.sampled_action)

    def get_logprob(self, action):
        return tf.reduce_sum(self.pd.log_prob(action), axis=-1)

    def act(self, observation):
        outputs = [self.sampled_action, self.logprob, self.vpred]
        a, l, v = tf.get_default_session().run(outputs, {
            self.observation_ph: observation })
        return a, l, v

    def save_model(self, gs):
        save_path = self.saver.save(self.sess, os.path.join(self.logdir, self.scope+'-model.ckpt'),
                                    global_step=gs)
        print("Model saved in path: %s" % save_path)
        return save_path
    
    def load_model(self, model_path):
        print("Loading model from %s" % model_path)
        self.saver.restore(self.sess, model_path)

