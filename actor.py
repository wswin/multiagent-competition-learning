import numpy as np


class Actor():
    def __init__(self, env, policies, horizon, gamma=0.99, lam=0.95):
        self.env = env
        self.policies = policies
        self.gamma = gamma
        self.horizon = horizon
        self.lam = lam
        self.frames = 0

    def _calculate_gae(self, values, rewards, masks):
        returns, advantages = np.zeros_like(rewards), np.zeros_like(rewards)
        deltas = rewards + self.gamma * values[1:] * masks - values[:-1]
        gae = 0
        for t in reversed(range(self.horizon)):
            advantages[t] = gae = deltas[t] + self.gamma * self.lam * masks[t] * gae
        returns = advantages + values[:-1]
        advantages = (advantages - np.mean(advantages))/(np.std(advantages) + 1e-8)
        return advantages, returns

    def run(self):
        """Probes training data from environment.
            Acts using policies and is limited by horizon.

            Returns:
                1st:
                    Tuple of 2 (for each policy(agent)) lists, each containg 6 values, i.e.:
                        observation of an environment,
                        action taken by agent in this environment,
                        log probability of that action,
                        value
                        advantage
                        return
                2nd:
                    Tuple of 2 (for each agent) total reward lists for complete episodes     
        """
        actions, logprobs, values = [[],[]], [[],[]], [[],[]]
        observations, rewards, masks, total_rewards = [[], []], [[],[]], [[],[]], [[],[]]
         
        total_reward = [0, 0]
        action, logprob, value = [[],[]], [[],[]], [[],[]]
        
        observation = self.env.reset()
        
        for t in range(self.horizon):
            self.frames += 1
            for a in range(len(self.policies)):
                observations[a].append(observation[a]) 
                # additional dummy axis because policy works on batches
                # todo: real batch with env used in parallel
                action[a], value[a], logprob[a] = self.policies[a].act(observation[a][None])
                action[a], value[a], logprob[a] = action[a][0], value[a][0], logprob[a][0]
                actions[a].append(action[a])
                logprobs[a].append(logprob[a])
                values[a].append(value[a])

            next_observation, reward, done, info = self.env.step(tuple(action))
 
            for a in range(len(self.policies)):
                rewards[a].append(reward[a])
                masks[a].append(np.invert(done[a]))
                total_reward[a] += reward[a]
                if done[a]:
                    total_rewards[a].append(total_reward[a])
                    total_reward[a] = 0
                    next_observation = self.env.reset()
            observation = next_observation

        advantages = [[], []]
        transitions = [[], []]
        returns = [[], []]
        for a in range(len(self.policies)):
            _, _, value[a] = self.policies[a].act(observation[a][None]) 
            values[a].append(value[a][0])

            observations[a], actions[a], logprobs[a], values[a], rewards[a], masks[a] = \
                map(np.array, [observations[a], actions[a], logprobs[a], values[a], rewards[a], masks[a]])

            advantages[a], returns[a] = self._calculate_gae(values[a], rewards[a], masks[a])

            transitions[a] = list(zip(observations[a], actions[a], logprobs[a],
                                values[a][:-1], advantages[a], returns[a]))

        return tuple(transitions), tuple(total_rewards)

