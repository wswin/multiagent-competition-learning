import sys
import argparse
import math
from operator import itemgetter

import tensorflow as tf
import numpy as np
import gym
import gym_compete

from policy import MlpPolicyValue
from critic import Critic
from actor import Actor
from logger import Logger
import utils


def parse_args():
    parser = argparse.ArgumentParser(description="Environments for Multi-agent competition")

    parser.add_argument("--env", default="run-to-goal-ants", type=str,
                        help="competitive environment: run-to-goal-humans, run-to-goal-ants,"\
                            " you-shall-not-pass, sumo-humans, sumo-ants, kick-and-defend")

    parser.add_argument("--timesteps", type=float, default=1e6, help="Number of timesteps.")
    
    parser.add_argument("--horizon", type=int, default=128, help="Number of steps actor takes in each iteration.")

    parser.add_argument("--epochs", default=3, type=int, help='Number of training epochs per update.')

    parser.add_argument("--batch_size", default=32, type=int, help="Batch size.")

    parser.add_argument("--lr", type=float, default=2.5e-4, help="Learning rate")

    parser.add_argument("--clip_param", type=float, default=0.1, help="Gradient clip value.")

    parser.add_argument("--logdir", type=str, help="Path to log directory.")
    
    parser.add_argument("--save_freq", type=int, default=5000, help="Number of updates before " \
                            "saving checkpoint")

    args = parser.parse_args()

    return args

def run(config):
    env = utils.get_env(config.env)

    ob_space = env.observation_space.spaces[0].shape
    ac_space = env.action_space.spaces[0].shape
    
    iterations = math.ceil(config.timesteps / config.horizon)

    tf_config = tf.ConfigProto(inter_op_parallelism_threads=1, intra_op_parallelism_threads=1)
    sess = tf.Session(config=tf_config)
    sess.__enter__()

    logger = Logger(sess, config.logdir)

    policies = []
    critics = []
    for a in range(2):
        if policy_type == "mlp":
            policy = MlpPolicyValue(sess=sess, scope="policy"+str(a),
                                    ob_space=ob_space, ac_space=ac_space,
                                    logdir=config.logdir, normalize=True)
        policies.append(policy)
        critic = Critic(ob_space, ac_space, policies[-1], scope="critic"+str(a))
        critics.append(critic)

    actor = Actor(env, policies, config.horizon)

    # initialize uninitialized variables
    sess.run(tf.variables_initializer(tf.global_variables()))

    # main training loop
    for i in range(iterations):
        transitions, total_rewards = actor.run()

        pg_losses = [[], []] 
        vf_losses = [[], []] 
        entropy_losses = [[], []] 
        total_losses = [[], []] 
        # each agent got its own optimizer
        for a in range(len(policies)): 
            indices = np.arange(len(transitions[a]))
            for e in range(config.epochs):
                np.random.shuffle(indices)
                for b in range(len(transitions[a])//config.batch_size):
                    batch_idx = indices[b*config.batch_size:(b+1)*config.batch_size]
                    batch = itemgetter(*batch_idx)(transitions[0])
                    pg_loss, vf_loss, entropy_loss, total_loss, gs = \
                        critics[a].update_policy(*tuple(map(np.array, zip(*batch))),
                            config.lr, config.clip_param)
                    pg_losses[a].append(pg_loss)
                    vf_losses[a].append(vf_loss)
                    entropy_losses[a].append(entropy_loss)
                    total_losses[a].append(total_loss)

                    if gs % config.save_freq == 0: critics[a].save_policy()

        mean_pg_loss = np.mean(np.array(pg_losses), axis=1)
        mean_vf_loss = np.mean(np.array(vf_losses), axis=1)
        mean_entropy_loss = np.mean(np.array(entropy_losses), axis=1)
        mean_total_loss = np.mean(np.array(total_losses), axis=1)
        logger.log_losses(mean_pg_loss, mean_vf_loss, mean_entropy_loss, mean_total_loss,
            config.lr, config.clip_param, actor.frames)
        logger.log_console(actor.frames)
        
        if actor.frames >= config.timesteps:
            model_path = critics[0].save_policy()
            model_path = critics[1].save_policy()
            break


if __name__ == "__main__":
    config = parse_args()
    run(config)

