import tensorflow as tf
import numpy as np
from tensorflow.contrib import layers
import gym


class Critic(object):
    def __init__(self, ob_space, ac_space, policy, scope):
        self.ob_space = ob_space
        self.ac_space = ac_space
        self.global_step = tf.Variable(0, trainable=False)
        self.policy = policy
        self._placeholders()
        self._loss()
 
    def _placeholders(self):
        self.action_old = tf.placeholder(tf.float32, (None, *self.ac_space), "action_old")
        self.logprob_old = tf.placeholder(tf.float32, (None, ), "logprob_old")
        self.value_old = tf.placeholder(tf.float32, (None, ), "value_old")
        self.advantage = tf.placeholder(tf.float32, (None, ), "advantage")
        self.total_return = tf.placeholder(tf.float32, (None, ), "total_return")
        self.lr = tf.placeholder(tf.float32, None, "lr")
        self.clip_param = tf.placeholder(tf.float32, None, "clip_param")

    def _loss(self):
        ratio = tf.exp(self.policy.get_logprob(self.action_old) - self.logprob_old)
        pg_loss1 = self.advantage*ratio
        pg_loss2 = self.advantage*tf.clip_by_value(ratio, 1-self.clip_param,1+self.clip_param)
        self.pg_loss = -tf.reduce_mean(tf.minimum(pg_loss1, pg_loss2)) #fit so -
        
        vf_loss_1 = tf.losses.mean_squared_error(self.total_return, self.policy.vpred)
        vf_clipped = self.value_old + tf.clip_by_value(self.policy.vpred - self.value_old, -self.clip_param, self.clip_param)
        vf_loss_2 = tf.losses.mean_squared_error(self.total_return, vf_clipped)
        self.vf_loss = tf.reduce_mean(tf.maximum(vf_loss_1, vf_loss_2))

        self.entropy_loss = tf.reduce_mean(self.policy.pd.entropy())

        self.loss = self.pg_loss + 0.5 * self.vf_loss# - self.entropy_coef * self.entropy_loss
        params = tf.trainable_variables()
        optimizer = tf.train.AdamOptimizer(self.lr, epsilon=1e-5)
        grads_and_var = optimizer.compute_gradients(self.loss, params)
        grads, var = zip(*grads_and_var)
        grads, _grad_norm = tf.clip_by_global_norm(grads, 0.5)
        self.train_op = optimizer.apply_gradients(list(zip(grads, var)), global_step=self.global_step)

    def update_policy(self, observation, action_old, logprob_old, value_old, advantage, total_return,
            lr, clip_param):
        pg_loss, vf_loss, entropy_loss, loss, _, gs = \
            tf.get_default_session().run(
            [self.pg_loss,
            self.vf_loss,
            self.entropy_loss,
            self.loss,
            self.train_op,
            self.global_step],
            feed_dict={
                self.policy.observation_ph: observation,
                self.action_old: action_old,
                self.logprob_old: logprob_old,
                self.value_old: value_old,
                self.advantage: advantage,
                self.total_return: total_return,
                self.lr: lr,
                self.clip_param: clip_param
            })

        return pg_loss, vf_loss, entropy_loss, loss, gs

    def save_policy(self):
        self.policy.save_model(self.global_step)

